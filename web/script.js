$(function(){
	if ($('.marquee').text().length > 0) {
		$('.marquee').marquee({
			duration: 9000
		});
	}
});
	
if ("WebSocket" in window) {

	var sign_dict = {
		"Mills": "\u00b0",
		"Ambient Temperature": "\u00b0",
		"Bins": "%"
	};

	// Let us open a web socket
	const socket = new WebSocket("wss://seraphim.castlevalleymill.com:8000");

	socket.addEventListener('message', function (event) {
	    	var message = JSON.parse(event.data.replace(/'/g, `"`));
	    	$("h3").each(function(index){
	    		if ($(this).text().trim() === message[0]) {
	    			var value = message[1] + sign_dict[$(this).parent().parent().parent().children().first().text().trim()]
	    			$(this).parent().next().text(value);
	    		}
    		});
	});
} else {
	window.alert("Websockets are not supported. This webpage WILL NOT WORK!");
}
