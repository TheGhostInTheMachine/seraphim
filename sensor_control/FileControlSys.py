from sensor_control.Sensor import Sensor
import time
def importSensorConfiguration():
        sensConfig = open("/home/pi/Desktop/seraphim-master/sensor_control/SensorConfiguration.txt","r")
        sList = []
        for line in sensConfig:
                # sensorText:SID:EID1:Type1:EID2:Type2:EID3:Type3
                configLine = line.split(":")
                sList.append(Sensor(configLine[1],configLine[0],int(configLine[2]),int(configLine[3]),int(configLine[4]),int(configLine[5]),int(configLine[6]),int(configLine[7])))
        sensConfig.close()
        return sList

def exportSensorConfiguration(sList):
	sensConfig = open("/home/pi/Desktop/seraphim-master/sensor_control/SensorConfiguration.txt","w")
	for element in sList:
		configString = ""
		configString += element.text+":"
		configString += element.SID+":"
		configString += element.EID+":"
		configString += element.TS1+":"
		configString += element.TS2+":"
		configString += element.TS3+"\n"
	sensConfig.close()

def writeToSensorFile(sensorID, data, battVolt):
	file = "/home/pi/Desktop/seraphim-master/sensor_control/SensorData/sensor_"+sensorID+"_data.txt"
	timeStamp = time.strftime('%Y_%m_%d_%H_%M_%S')
	sensFile = open(file,"a")
	sensFile.write(timeStamp+","+battVolt+","+data+"\n")
	sensFile.close()

def importBinHeightConfig():
        binConfig = open("/home/pi/Desktop/seraphim-master/sensor_control/binConfiguration.txt","r")
        binTList = []
        
        class specialVariable():
                def __init__(self,sensorid,port,special):
                        self.sensorID = sensorid
                        self.pinNum = port
                        self.specialVar = special
                        
                
        for line in binConfig:
                # sensor num, data marker, bin height
                configLine = line.split(":")
                binTList.append(specialVariable(configLine[0],configLine[1],configLine[2]))
        binConfig.close()
        return binTList

