from collections import deque


class TransferManager():
	def __init__(self):
		self.transferQueue = deque(maxlen=10)
		self.alertQueue = deque(maxlen=10)

	def addTransferQueue(self, ids, datas):
		self.transferQueue.append([ids, datas])

	def addAlertQueue(self, alert):
		self.alertQueue.append(alert)
