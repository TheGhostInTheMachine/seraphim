import logging
import json
import time
from threading import Thread
from SimpleWebSocketServer import SimpleSSLWebSocketServer, WebSocket

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.INFO)


def handle_clients(transfer_manager, eids):
	while True:
		time.sleep(1)
		for data in transfer_manager.transferQueue:
			converted_data = [eids[str(data[0])], int(data[1])]
			for client in WebSocketClient.clients:
				client.sendMessage(str(converted_data))


class WebSocketClient(WebSocket):

	clients = []
	total_clients = 0

	def __init__(self, server, sock, address):
		super().__init__(server, sock, address)
		self.id = None

	def handleConnected(self):
		WebSocketClient.clients.append(self)
		self.id = WebSocketClient.total_clients
		logger.info(f"Client {self.id} has connected")
		WebSocketClient.total_clients += 1

	def handleClose(self):
		logger.info(f"Client {self.id} has disconnected")
		WebSocketClient.clients.remove(self)


def run(transfer_manager, port=8000):
	with open("id.json", "r") as ids:
		eids = json.load(ids)

	server = SimpleSSLWebSocketServer('0.0.0.0', port, WebSocketClient, ".ssl/site.crt", ".ssl/site.key")
	Thread(target=server.serveforever, daemon=True).start()
	logger.info(f"Listening for WebSocket connections on {port}...")

	handle_clients(transfer_manager, eids)
