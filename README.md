# Overview

Project Seraphim is to be used to keep track of all the sensors in the mill. The GUI and I2C is written in python, while the microchips that control the sensors are programmed in C. The C code can and probably will be pushed here when it is done, but ta the time of writing, this repo is pure python (remove this when C code comes on). The code here is annotated for your viewing pleasure, with comment blocks prepending every major algorithm to explain what it's for.

# Code style

I tried to keep my style as consistent as possible, using underscores for variables `my_variable` and not camelCase `myVariable`. I used tabs converted into 4 spaces by convention, but If you're maintaining this, it really doesn't matter as long as the code is readable and formatted nicely.