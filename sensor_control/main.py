import sensor_control.FileControlSys as FileControlSys
import sensor_control.sensorfunctions as sensorfunctions

import sensor_control.Sensor as Sensor

from serial import Serial
import time

# Version 1.0 6/12/2018 Launch


def run(transferManager=None):
	serialPort = Serial('/dev/ttyUSB0', 115200)
	sensorList = FileControlSys.importSensorConfiguration()
	binList = FileControlSys.importBinHeightConfig()
	transfer = transferManager
	#transfer.addTransferQueue(1, 1)

	while True:
		# id:batt:d1:d2:d3
		responseE = serialPort.readline()
		if(responseE):
			if "Version" not in str(responseE):
				print("=======================================================")
				response = responseE.decode('UTF-8')
				# response = str(responseE.split())
				timeStamp = time.strftime('%Y/%m/%d %H:%M:%S')
				print(timeStamp + " --- " + str(response))
				responseList = response.split(":")

				for i in sensorList:
					if(i.SID == responseList[0]):
						special1 = 1
						special2 = 1
						special3 = 1
						# print(i.SID)
						for j in binList:
							# print(j.sensorID)
							if(int(i.SID) == int(j.sensorID)):
								if(int(j.pinNum) == 1):
									special1 = j.specialVar
								if(int(j.pinNum) == 2):
									special2 = j.specialVar
								if(int(j.pinNum) == 3):
									special3 = j.specialVar

						i.battVoltage = responseList[1]
						response2 = 0.0
						response3 = 0.0
						response4 = 0.0
						if "NAN" not in responseList[2]:
							response2 = float(responseList[2])
						if "NAN" not in responseList[3]:
							response3 = float(responseList[3])
						if "NAN" not in responseList[4]:
							response4 = float(responseList[4])

						odata1 = response2
						odata2 = response3
						odata3 = response4

						# print(special1)
						# print(special2)
						# print(special3)

						fdata1 = sensorfunctions.doSensorFunction(response2, i.TS1, special1)
						fdata2 = sensorfunctions.doSensorFunction(response3, i.TS2, special2)
						fdata3 = sensorfunctions.doSensorFunction(response4, i.TS3, special3)

						i.updateSensorInfo(fdata1, fdata2, fdata3, odata1, odata2, odata3)

						#eidArray = 
						if transferManager is not None:
							if i.EID1 != 0:
								transfer.addTransferQueue(i.EID1, fdata1)
							if i.EID2 != 0:
								transfer.addTransferQueue(i.EID2, fdata2)
							if i.EID3 != 0:
								transfer.addTransferQueue(i.EID3, fdata3)

				for i in sensorList:
					i.printSensorData()

	#        commandInput = input()
	#        if(commandInput):
	#                print(commandInput)


if __name__ == "__main__":
	run()
