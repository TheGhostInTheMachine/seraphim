from bs4 import BeautifulSoup as Soup
from xml.etree import ElementTree as ET

import json
import logging
import sys

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.INFO)


def read_config():
	try:
		with open("config.json") as config:
			return json.load(config)
	except FileNotFoundError:
		logging.error("Config file not found! Aborting!")
		sys.exit(1)


def better_pretty(line_list):
	for i in range(len(line_list)):
		indent_level = 0
		while line_list[i].startswith(" "):
			line_list[i] = line_list[i][1:]
			indent_level += 1
		line_list[i] = ("\t" * indent_level) + line_list[i]
	return "\n".join(line_list)


def generate_html():
	config_data = read_config()
	generated_html = {}
	for key in config_data:
		for monitor_config in config_data[key]:
			monitor_container = ET.Element("div", attrib={"class": "monitor-container"})
			monitor_label = ET.Element("div", attrib={"class": "monitor-label"})
			h3 = ET.Element("h3")
			h3.text = monitor_config
			monitor = ET.Element("div", attrib={"class": "monitor"})

			if key == "Bins":
				monitor.text = "0%"
			elif key == "Mills" or key == "Ambient Temperature":
				monitor.text = "0\u00b0"

			monitor_label.append(h3)
			monitor_container.append(monitor_label)
			monitor_container.append(monitor)

			if key in generated_html:
				generated_html[key] += ET.tostring(monitor_container, encoding="unicode", method="html")
			else:
				generated_html[key] = ET.tostring(monitor_container, encoding="unicode", method="html")

	return generated_html


def insert_html(dynamic_html_dict):
	with open("web/index.html", "r") as file:
		main_soup = Soup(file.read(), "html.parser")

	for key in dynamic_html_dict:
		dynamic_html_dict[key] = Soup(dynamic_html_dict[key], "html.parser")

	for header in main_soup.find_all("h1"):
		if "Bins" in header.getText():
			header.insert_after(dynamic_html_dict["Bins"])
		elif "Mills" in header.getText():
			header.insert_after(dynamic_html_dict["Mills"])
		elif "Ambient Temperature" in header.getText():
			header.insert_after(dynamic_html_dict["Ambient Temperature"])

	with open("web/index.html", "w") as file:
		file.write(better_pretty(main_soup.prettify().split("\n")))


def remove_html():
	with open("web/index.html", "r") as file:
		soup = Soup(file.read(), "html.parser")

	[x.extract() for x in soup.find_all("div", {"class": "monitor-container"})]

	with open("web/index.html", "w") as file:
		file.write(better_pretty(soup.prettify().split("\n")))
