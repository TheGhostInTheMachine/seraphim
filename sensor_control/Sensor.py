from sensor_control import FileControlSys

class Sensor():
        def __init__(self, sensorID, sensorText,elementID1,typeSensor1, elementID2,typeSensor2, elementID3, typeSensor3):
                self.SID = sensorID 
                self.EID1 = elementID1
                self.EID2 = elementID2
                self.EID3 = elementID3
                self.text = sensorText
                self.TS1 = typeSensor1
                self.TS2 = typeSensor2 
                self.TS3 = typeSensor3
                self.battVoltage = 0
                self.data1 = -1
                self.data2 = -1
                self.data3 = -1
                self.count = -1 #holds count for average sd write
                self.odata1 = -1
                self.odata2 = -1
                self.odata3 = -1
                self.avD1 = 0.0
                self.avD2 = 0.0
                self.avD3 = 0.0

        def updateSensorInfo(self,d1,d2,d3,o1,o2,o3):
                self.count += 1
                self.data1 = d1
                self.data2 = d2
                self.data3 = d3
                self.odata1 = o1
                self.odata2 = o2
                self.odata3 = o3
                self.avD1 = self.avD1 + d1
                self.avD2 = self.avD2 + d2
                self.avD3 = self.avD3 + d3
                if(self.count > 9):
                        self.count = 0
                        self.send = ""
                        self.send+=str(self.avD1/10)+","
                        self.send+=str(self.avD2/10)+","
                        self.send+=str(self.avD3/10)+":"
                        self.send+=str(self.odata1)+","
                        self.send+=str(self.odata2)+","
                        self.send+=str(self.odata3)
                        self.avD1 = 0.0
                        self.avD2 = 0.0
                        self.avD3 = 0.0
                        FileControlSys.writeToSensorFile(self.SID,self.send,self.battVoltage)

        def printSensorData(self):
                t = ""
                t += self.SID+" : "
                t += self.text+" : "
                t += str(self.battVoltage)+" : "
                t += str(self.data1)+" : "
                t += str(self.data2)+" : "
                t += str(self.data3)
                print(t)

                
